
class AgentCharlie::Action::TSort

    def initialize(manifest = nil)
        @manifest = manifest
        AgentCharlie::Logger.debug{"Initializing TSort action with manifest '#{manifest}'"}
    end

    def run_action(progress)
        @progress = progress
        charlie = AgentCharlie::Charlie.instance

        # First thing to do is check if we have root
        if Process::Sys.geteuid != 0
            AgentCharlie::Logger.debug{'Root access was missing so we requested it'}
            
            # Create a "sudo request" action, and then suspend this action for later
            ENV['GEM_HOME'] = AgentCharlie.default_gem_home
            ENV['GEM_PATH'] = AgentCharlie.default_gem_path
            charlie.ui.sudo('Password: ', 'ruby', AgentCharlie.script_file.to_s, *AgentCharlie.original_args)

            # KABOOM
            AgentCharlie::Logger.error{'SHOULD NOT GET HERE'}
        end

        # Check setup vs install
        if @manifest
            # Ensure manifest exists
            if charlie.manifest_collection.get_manifest(@manifest) == nil
                progress.failed("Manifest #{@manifest} does not exist")
                return
            end

            # Okay.
            setupList = charlie.manifest_collection.get_specific_tsorted_manifests(@manifest)
        else
            setupList = charlie.manifest_collection.get_tsorted_manifests
        end

        progress.complete("Dependencies resolved", setupList.map do |manifest|
            charlie.manifest_collection.get_manifest(manifest)
        end)
    end

    def get_progress
        return @progress
    end

end
