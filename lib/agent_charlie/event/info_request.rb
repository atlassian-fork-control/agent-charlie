class AgentCharlie::Event::InfoRequest

    attr_accessor :prompt
    attr_accessor :verify
    attr_accessor :default

    def initialize(prompt, default = nil, information = nil, &verify)
        @prompt = prompt
        @verify = verify
        @default = default
    end

    def verify_result(result)
        raise "Implement me"
    end

end

class AgentCharlie::Event::StringInfoRequest < AgentCharlie::Event::InfoRequest
    def verify_result(result)
        return result.is_a?(String)
    end
end

class AgentCharlie::Event::PasswordInfoRequest < AgentCharlie::Event::InfoRequest
    def verify_result(result)
        return result.is_a?(String)
    end
end

class AgentCharlie::Event::BooleanInfoRequest < AgentCharlie::Event::InfoRequest
    def verify_result(result)
        return result.is_a?(TrueClass) || result.is_a?(FalseClass)
    end
end


AgentCharlie::Event.register_event('Information Request', AgentCharlie::Event::InfoRequest)
AgentCharlie::Event.register_event('Password Information Request', AgentCharlie::Event::StringInfoRequest)
AgentCharlie::Event.register_event('String Information Request', AgentCharlie::Event::PasswordInfoRequest)
AgentCharlie::Event.register_event('Boolean Information Request', AgentCharlie::Event::BooleanInfoRequest)
