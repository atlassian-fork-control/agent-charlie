require 'logger'
require 'puppet'
require 'puppet/util/log'

module AgentCharlie::Logger
  @prog_name = 'Agent Charlie'

  @logger = ::Logger.new(STDOUT)
  @logger.level = Logger::INFO
  Puppet::Util::Log.level = :err
  
  def self.info(&block)
    @logger.info(&block)
  end

  def self.debug(&block)
    @logger.debug(&block)
  end

  def self.error(&block)
    @logger.error(&block)
  end

  def self.fatal(&block)
    @logger.fatal(&block)
  end

  def self.turn_on_debug
    @logger.level = Logger::DEBUG
    Puppet::Util::Log.level = :notice
  end
end