
class AgentCharlie::InstallMethod

    @method_classes = {}

    class Base
        def parse(data)
            raise 'Implement parse'
        end

        def description
            raise 'Implement me. Return a description.'
        end
    end

    def self.new_method(method_cls, method_name)
        @method_classes[method_name] = method_cls
    end

    def self.get_method(method_name, data)
	    (method_class = @method_classes[method_name]) or return nil
        obj = method_class.new
        obj.parse(data)
        return obj
    end
end

Dir[File.join(File.expand_path('lib/agent_charlie/install_method', AgentCharlie.charlie_root), '*.rb')].each do |file|
    require 'agent_charlie/install_method/' + File.basename(file, File.extname(file))
end