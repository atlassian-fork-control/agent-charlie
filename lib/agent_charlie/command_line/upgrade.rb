
require 'agent_charlie/action'
require 'trollop'

class AgentCharlie::CommandLine::Upgrade < AgentCharlie::CommandLine::Base
    def initialize
        super("upgrade", "Upgrade")
    end

    def execute(args)
        charlie = AgentCharlie::Charlie.instance

        subcommand_opts = Trollop::options(args) do
            banner <<-EOS.gsub(/^\t*/, '')
				Upgrades Agent Charlie

				Usage:
				   #{$0} ... upgrade [options]

				Options:
				EOS

            opt :branch, "Selects a different branch to upgrade using", :short => '-b', :type => :string, :default => "stable"
        end

        if args.length != 0
            Trollop::die "Unknown additional arguments: #{args.inspect}"
        end

        charlie.add_action(AgentCharlie::Action::Upgrade.new({
            :branch => subcommand_opts[:branch]
        })) do |progress|
            if progress.stage == :FINISHED
                AgentCharlie::Logger.info{progress.message}
            elsif progress.stage == :FAILED
                AgentCharlie::Logger.error{progress.message}
            elsif progress.stage == :RUNNING
                AgentCharlie::Logger.debug{progress.message}
            end
        end
    end
end

AgentCharlie::CommandLine.newCommand(AgentCharlie::CommandLine::Upgrade)