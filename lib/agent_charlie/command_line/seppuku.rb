
require 'agent_charlie/action'
require 'trollop'

class AgentCharlie::CommandLine::Seppuku < AgentCharlie::CommandLine::Base
    def initialize
        super("seppuku", "Seppuku")
    end

    def execute(args)
        charlie = AgentCharlie::Charlie.instance

        subcommand_opts = Trollop::options(args) do
            banner <<-EOS.gsub(/^\t*/, '')
				Orders Agent Charlie to honorably remove itself from your PC

				Usage:
				   #{$0} ... seppuku [options]

				Options:
				EOS
        end

        if args.length != 0
            Trollop::die "Unknown additional arguments: #{args.inspect}"
        end

        raise "Not yet implemented"
    end
end

AgentCharlie::CommandLine.newCommand(AgentCharlie::CommandLine::Seppuku)