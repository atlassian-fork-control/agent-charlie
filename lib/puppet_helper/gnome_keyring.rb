require 'puppet_helper/charlie_keyring'

require 'ffi'

if not Object.const_defined?('GnomeKeyring')
    class GnomeKeyring < CharlieKeyring
        def initialize(displayName, name)
            @displayName = displayName
            @name = "charlie_" + name

            @passwordSchema = GnomeKeyringFFI::GnomeKeyringPasswordSchema.new
            @passwordSchema[:item_type] = :GNOME_KEYRING_ITEM_GENERIC_SECRET
            @passwordSchema[:attributes][0][:name] = FFI::MemoryPointer.from_string("shortName")
            @passwordSchema[:attributes][0][:type] = :GNOME_KEYRING_ATTRIBUTE_TYPE_STRING
            @passwordSchema[:attributes][1][:name] = FFI::MemoryPointer.from_string("username")
            @passwordSchema[:attributes][1][:type] = :GNOME_KEYRING_ATTRIBUTE_TYPE_STRING
            @passwordSchema[:attributes][2][:name] = nil
            @passwordSchema[:attributes][2][:type] = :GNOME_KEYRING_ATTRIBUTE_TYPE_STRING
        end

        def storePassword(username, password)
            # Delete all other charlie_ passwords
            GnomeKeyringFFI.gnome_keyring_delete_password_sync(@passwordSchema, :string, "shortName", :string, @name, :pointer, nil)

            # Store the password
            result = GnomeKeyringFFI.gnome_keyring_store_password_sync(@passwordSchema, nil, @displayName, password, :string, "shortName", :string, @name, :string, "username", :string, username, :pointer, nil)
            return result == :GNOME_KEYRING_RESULT_OK
        end

        def refreshFromKeyring()
            # Find the item that matches our criteria
            itemFoundListPtrPtr = FFI::MemoryPointer.new(:pointer, 1)
            result = GnomeKeyringFFI.gnome_keyring_find_itemsv_sync(:GNOME_KEYRING_ITEM_GENERIC_SECRET, itemFoundListPtrPtr, :string, "shortName", :GnomeKeyringAttributeType, :GNOME_KEYRING_ATTRIBUTE_TYPE_STRING, :string, @name, :pointer, nil)
            if result != :GNOME_KEYRING_RESULT_OK
                return false
            end

            # Conversion to the proper data type
            itemFoundListPtr = itemFoundListPtrPtr.read_pointer()
            if itemFoundListPtr.null?
                return false
            end

            itemFoundList = GnomeKeyringFFI::GList.new itemFoundListPtr
            item = GnomeKeyringFFI::GnomeKeyringFound.new itemFoundList[:data]

            # Get the attribute list
            attributeListPtrPtr = FFI::MemoryPointer.new(:pointer, 1)
            keyringString = (item[:keyring].null? ? nil : item[:keyring].read_string())
            result = GnomeKeyringFFI.gnome_keyring_item_get_attributes_sync(keyringString, item[:item_id], attributeListPtrPtr)
            if result != :GNOME_KEYRING_RESULT_OK
                GnomeKeyringFFI.gnome_keyring_found_list_free(itemFoundListPtr)
                return false
            end

            attributeListPtr = attributeListPtrPtr.read_pointer()
            if attributeListPtr.null?
                GnomeKeyringFFI.gnome_keyring_found_list_free(itemFoundListPtr)
                return false
            end

            attributeList = GnomeKeyringFFI::GnomeKeyringAttributeList.new attributeListPtr

            # Read all the attributes and passwords and stuff
            keyringItemInfoPtrPtr = FFI::MemoryPointer.new(:pointer, 1)
            result = GnomeKeyringFFI::gnome_keyring_item_get_info_sync(keyringString, item[:item_id], keyringItemInfoPtrPtr)
            if result != :GNOME_KEYRING_RESULT_OK
                GnomeKeyringFFI.gnome_keyring_attribute_list_free(attributeListPtr)
                GnomeKeyringFFI.gnome_keyring_found_list_free(itemFoundListPtr)
                return false
            end

            keyringItemInfoPtr = keyringItemInfoPtrPtr.read_pointer();
            if keyringItemInfoPtr.null?
                GnomeKeyringFFI.gnome_keyring_attribute_list_free(attributeListPtr)
                GnomeKeyringFFI.gnome_keyring_found_list_free(itemFoundListPtr)
                return false
            end

            secret = GnomeKeyringFFI::gnome_keyring_item_info_get_secret(keyringItemInfoPtr)
            @password = secret.read_string()

            0.upto(attributeList[:len]-1) do |i|
                attribute = GnomeKeyringFFI::GnomeKeyringAttribute.new(attributeList[:data] + i*GnomeKeyringFFI::GnomeKeyringAttribute.size)

                if attribute[:name].read_string() == "username"
                    @username = attribute[:value][:string].read_string()
                end
            end

            # Free itemFoundList and the attributeList and keyring info
            GnomeKeyringFFI.gnome_keyring_item_info_free(keyringItemInfoPtr)
            GnomeKeyringFFI.gnome_keyring_attribute_list_free(attributeListPtr)
            GnomeKeyringFFI.gnome_keyring_found_list_free(itemFoundListPtr)

            # Return something
            return true
        end

        def retrieveUsername()
            @username
        end

        def retrievePassword()
            @password
        end
    end
end

module GnomeKeyringFFI
    extend FFI::Library

    ffi_lib "libgnome-keyring.so"

    enum :GnomeKeyringResult, [
        :GNOME_KEYRING_RESULT_OK,
        :GNOME_KEYRING_RESULT_DENIED,
        :GNOME_KEYRING_RESULT_NO_KEYRING_DAEMON,
        :GNOME_KEYRING_RESULT_ALREADY_UNLOCKED,
        :GNOME_KEYRING_RESULT_NO_SUCH_KEYRING,
        :GNOME_KEYRING_RESULT_BAD_ARGUMENTS,
        :GNOME_KEYRING_RESULT_IO_ERROR,
        :GNOME_KEYRING_RESULT_CANCELLED,
        :GNOME_KEYRING_RESULT_KEYRING_ALREADY_EXISTS,
        :GNOME_KEYRING_RESULT_NO_MATCH
    ]

    enum :GnomeKeyringAttributeType, [
        :GNOME_KEYRING_ATTRIBUTE_TYPE_STRING,
        :GNOME_KEYRING_ATTRIBUTE_TYPE_UINT32
    ]

    enum :GnomeKeyringItemType, [
        :GNOME_KEYRING_ITEM_GENERIC_SECRET, 0,
        :GNOME_KEYRING_ITEM_NETWORK_PASSWORD,
        :GNOME_KEYRING_ITEM_NOTE,
        :GNOME_KEYRING_ITEM_CHAINED_KEYRING_PASSWORD,
        :GNOME_KEYRING_ITEM_ENCRYPTION_KEY_PASSWORD,
        :GNOME_KEYRING_ITEM_PK_STORAGE, 0x100,
        :GNOME_KEYRING_ITEM_LAST_TYPE
    ]

    class GnomeKeyringPasswordSchema_inner1 < FFI::Struct
        layout  :name,          :pointer, # char *
                :type,          :GnomeKeyringAttributeType
    end

    class GnomeKeyringPasswordSchema < FFI::Struct
        layout  :item_type,     :GnomeKeyringItemType,
                :attributes,    [GnomeKeyringPasswordSchema_inner1, 32]
    end

    class GnomeKeyringAttribute_inner1 < FFI::Union
        layout  :string,        :pointer,
                :integer,       :uint32
    end

    class GnomeKeyringAttribute < FFI::Struct
        layout  :name,          :pointer, # char *
                :type,          :GnomeKeyringAttributeType,
                :value,         GnomeKeyringAttribute_inner1
    end

    class GnomeKeyringAttributeList < FFI::Struct
        layout  :data,          :pointer, # char * (byte data)
                :len,           :uint
    end

    class GnomeKeyringFound < FFI::Struct
        layout  :keyring,       :pointer, # char *
                :item_id,       :uint, # id
                :attributes,    GnomeKeyringAttributeList.ptr,
                :secret,        :pointer # char *
    end

    class GList < FFI::Struct
        layout  :data,          :pointer,
                :next,          GList.ptr,
                :prev,          GList.ptr
    end

    callback :GnomeKeyringOperationDoneCallback, [:GnomeKeyringResult, :pointer], :void
    callback :GnomeKeyringOperationGetStringCallback, [:GnomeKeyringResult, :string, :pointer], :void
    callback :GDestroyNotify, [:pointer], :void

    # gpointer gnome_keyring_store_password (const GnomeKeyringPasswordSchema *schema,
    #                                        const gchar *keyring,
    #                                        const gchar *display_name,
    #                                        const gchar *password,
    #                                        GnomeKeyringOperationDoneCallback callback,
    #                                        gpointer data,
    #                                        GDestroyNotify destroy_data,
    #                                        ...);
    attach_function :gnome_keyring_store_password, [GnomeKeyringPasswordSchema.ptr, :string, :string, :string, :GnomeKeyringOperationDoneCallback, :pointer, :GDestroyNotify, :varargs], :pointer
    
    # GnomeKeyringResult gnome_keyring_store_password_sync (const GnomeKeyringPasswordSchema *schema,
    #                                                       const gchar *keyring,
    #                                                       const gchar *display_name,
    #                                                       const gchar *password,
    #                                                       ...);
    attach_function :gnome_keyring_store_password_sync, [GnomeKeyringPasswordSchema.ptr, :string, :string, :string, :varargs], :GnomeKeyringResult
    
    # gpointer gnome_keyring_find_password (const GnomeKeyringPasswordSchema *schema,
    #                                       GnomeKeyringOperationGetStringCallback callback,
    #                                       gpointer data,
    #                                       GDestroyNotify destroy_data,
    #                                       ...);
    attach_function :gnome_keyring_find_password, [GnomeKeyringPasswordSchema.ptr, :GnomeKeyringOperationGetStringCallback, :pointer, :GDestroyNotify, :varargs], :pointer

    # GnomeKeyringResult gnome_keyring_find_password_sync (const GnomeKeyringPasswordSchema *schema,
    #                                                      gchar **password,
    #                                                      ...);
    attach_function :gnome_keyring_find_password_sync, [GnomeKeyringPasswordSchema.ptr, :pointer, :varargs], :GnomeKeyringResult

    # gpointer gnome_keyring_delete_password (const GnomeKeyringPasswordSchema *schema,
    #                                         GnomeKeyringOperationDoneCallback callback,
    #                                         gpointer data,
    #                                         GDestroyNotify destroy_data,
    #                                         ...);
    attach_function :gnome_keyring_delete_password, [GnomeKeyringPasswordSchema.ptr, :GnomeKeyringOperationDoneCallback, :pointer, :GDestroyNotify], :pointer

    # GnomeKeyringResult gnome_keyring_delete_password_sync (const GnomeKeyringPasswordSchema *schema,
    #                                                        ...);
    attach_function :gnome_keyring_delete_password_sync, [GnomeKeyringPasswordSchema.ptr, :varargs], :GnomeKeyringResult

    # void gnome_keyring_free_password (gchar *password);
    attach_function :gnome_keyring_free_password, [:pointer], :void

    #
    # KEYRING INFO
    #
    # GnomeKeyringResult gnome_keyring_item_get_info_sync (const char *keyring,
    #                                                     guint32 id,
    #                                                     GnomeKeyringItemInfo **info);
    attach_function :gnome_keyring_item_get_info_sync, [:string, :uint32, :pointer], :GnomeKeyringResult

    # char * gnome_keyring_item_info_get_secret (GnomeKeyringItemInfo *item_info);
    attach_function :gnome_keyring_item_info_get_secret, [:pointer], :pointer

    # void gnome_keyring_item_info_free (GnomeKeyringItemInfo *item_info);
    attach_function :gnome_keyring_item_info_free, [:pointer], :void


    #
    # KEYRING ITEMS
    #
    #GnomeKeyringResult  gnome_keyring_item_get_attributes_sync
    #                                                   (const char *keyring,
    #                                                   guint32 id,
    #                                                   GnomeKeyringAttributeList **attributes);
    attach_function :gnome_keyring_item_get_attributes_sync, [:string, :uint32, :pointer], :GnomeKeyringResult

    # void gnome_keyring_attribute_list_free (GnomeKeyringAttributeList* attributes)
    attach_function :gnome_keyring_attribute_list_free, [:pointer], :void

    #
    # SEARCHING
    #
    #GnomeKeyringResult  gnome_keyring_find_items_sync (GnomeKeyringItemType type,
    #                                                  GnomeKeyringAttributeList *attributes,
    #                                                  GList **found);
    attach_function :gnome_keyring_find_items_sync, [:GnomeKeyringItemType, GnomeKeyringAttributeList.ptr, :pointer], :GnomeKeyringResult

    #GnomeKeyringResult  gnome_keyring_find_itemsv_sync (GnomeKeyringItemType type,
    #                                                   GList **found,
    #                                                   ...);
    attach_function :gnome_keyring_find_itemsv_sync, [:GnomeKeyringItemType, :pointer, :varargs], :GnomeKeyringResult

    # void                gnome_keyring_found_list_free       (GList *found_list);
    attach_function :gnome_keyring_found_list_free, [:pointer], :void

end