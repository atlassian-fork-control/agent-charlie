require 'thread'
require 'singleton'

require 'facter'
require 'open-uri'

# if Facter.operatingsystem == "Darwin"
#     require 'osx_keychain'
# elsif Facter.operatingsystem == "Ubuntu"
#     require 'gnome_keyring'
# end

if not Object.const_defined?('GenericLoginSingleton')
    class GenericLoginSingleton
        include Singleton

        def initialize(serviceName, usernamePrompt = "username")
            @serviceName = serviceName
            @username = nil
            @password = nil
            @usernamePrompt = usernamePrompt
            @mutex = Mutex.new

            # if Facter.operatingsystem == "Darwin"
            #     @keychain = OSXKeychain.new(serviceName, serviceName)
            # elsif Facter.operatingsystem == "Ubuntu"
            #     @keychain = GnomeKeyring.new(serviceName, serviceName)
            # end
        end

        def populateUsernamePassword
            @mutex.synchronize {
                # if @keychain.refreshFromKeyring()
                #     @username = @keychain.retrieveUsername()
                #     @password = @keychain.retrievePassword()
                # else
                #     getUserPass
                # end

                # We don't actually need to interact with the keyring now that Charlie runs all the manifests at once
                getUserPass

                while !authenticate
                    say("Username and/or password is wrong!")
                    @username = nil
                    @password = nil

                    getUserPass
                end

                # @keychain.storePassword(@username, @password)
            }
        end

        def getUserPass
            charlie = AgentCharlie::Charlie.instance

            if @username == nil || @password == nil
                @username = charlie.event.raise_event_single(AgentCharlie::Event::StringInfoRequest.new("#{@serviceName} #{@usernamePrompt}: ", nil, information) do |input|
                    true
                end)
                @password = charlie.event.raise_event_single(AgentCharlie::Event::PasswordInfoRequest.new("#{@serviceName} password: ") do |input|
                    true
                end)
            end
        end

        def information
          nil
        end

        def username
            populateUsernamePassword
            @username
        end

        def password
            populateUsernamePassword
            @password
        end
    end
end
