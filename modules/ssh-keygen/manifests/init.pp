class ssh-keygen {
    $user = $::config_user
    $group = $::config_group
    $user_home = $::config_home

    if ! ssh_key_exists() {
        $passphrase = read_ssh_password()
        $passphrase_flag = "-N '${passphrase}'"

        file {'ssh_directory':
            ensure  =>  directory,
            path    =>  "${user_home}/.ssh",
            owner   =>  $user,
            group   =>  $group,
            mode    =>  '0700',
        }

        # This command is run as root.
        exec {'generate_ssh_keys':
            command =>  "/usr/bin/sudo -u \"${user}\" /usr/bin/ssh-keygen -t rsa -C agent-charlie -f '${user_home}/.ssh/id_rsa' ${passphrase_flag}",
            creates =>  "${user_home}/.ssh/id_rsa",
            require =>  File['ssh_directory'],
        }
    } else {
        # Generate public key from private key
        exec {'generate_ssh_keys':
            command =>  "/usr/bin/ssh-keygen -y -f '${user_home}/.ssh/id_rsa' > '${user_home}/.ssh/id_rsa.pub'",
            creates =>  "${user_home}/.ssh/id_rsa.pub"
        }
    }

    file {'ssh_pub_key_ownership':
        path    =>  "${user_home}/.ssh/id_rsa.pub",
        owner   =>  $user,
        group   =>  $group,
        mode    =>  '0644',
        require =>  Exec['generate_ssh_keys'],
    }

    file {'ssh_key_ownership':
        path    =>  "${user_home}/.ssh/id_rsa",
        owner   =>  $user,
        group   =>  $group,
        mode    =>  '0400',
        require =>  Exec['generate_ssh_keys'],
    }
}
