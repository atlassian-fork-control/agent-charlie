# == Class: intellij::ubuntu
#
# Sets up intellij for ubuntu

class idea::intellij::ubuntu () {
    $repository = $::charlie_repository

    $local_file = 'ideaIU12_latest.tar.gz'
    $remote_file = "${repository}/ideaIU12_latest.tar.gz"
    $user_home = $::config_home
    $user = $::config_user
    $group = $::config_group

    $config_home = "${user_home}/.IntelliJIdea12"
    $key_file = 'idea12.key'

    # Might want to abstract these out
    $charlie_dir = $::config_charlie_home
    $target = $::config_atlassian_home
    $dir = "${target}/intellij12"
    $tmp = $::config_tmp_directory

    $release = 12

    file{[$user_home,
            "${user_home}/.local",
            "${user_home}/.local/share",
            "${user_home}/.local/share/applications"]:
        ensure  =>  directory,
        owner   =>  $user,
        group   =>  $group,
        before  =>  File['intellij_desktop'],
    }

    file{'intellij_desktop':
        path    =>  "${user_home}/.local/share/applications/idea12.desktop",
        owner   =>  $user,
        group   =>  $group,
        mode    =>  '0755',
        content =>  template('idea/idea-desktop.erb'),
    }

    file{'intellij_directory':
        ensure  =>  directory,
        path    =>  $dir,
        owner   =>  $user,
        group   =>  $group,
        recurse =>  inf,
    }

    file{'tmp_directory':
        ensure  =>  directory,
        path    =>  $tmp,
        owner   =>  $user,
        group   =>  $group,
        recurse =>  inf,
    }

    exec{'intellij_get':
        command =>  "/usr/bin/curl -L -s -o '${tmp}/${local_file}' '${remote_file}'",
        creates =>  "${tmp}/${local_file}",
        require =>  File['tmp_directory'],
    }

    file {'intellij_ownership':
        path    =>  "${tmp}/${local_file}",
        owner   =>  $user,
        group   =>  $group,
        require =>  [Exec['intellij_get']],
        recurse =>  inf,
    }

    exec{'intellij_extract':
        cwd     =>  $dir,
        command => "sudo -u '${user}' tar -C '${dir}' --strip-components 1 -xf '${tmp}/${local_file}'",
        require =>  [Exec['intellij_get'],File['intellij_ownership'], File['intellij_directory']],
        creates =>  "${dir}/bin",
        path    =>  ['/bin','/usr/bin'],
    }

    # Set up IntelliJ Settings
    case $::architecture {
        /^.*64$/: {
            $config_filename = 'idea64.vmoptions'
        }
        /^.*86$/: {
            $config_filename = 'idea.vmoptions'
        }
        default: {
            fail ('Unknown hardware architecture')
        }
    }

    file {
        [ $config_home, "${config_home}/config" ]:
        ensure  =>  directory,
        owner   =>  $user,
        group   =>  $group,
        before  =>  Exec['intellij_license'],
        recurse =>  inf,
    }

    exec {'intellij_license':
        command =>  "/usr/bin/curl -L -s -o '${config_home}/config/${key_file}' '${repository}/${key_file}'",
        require =>  Exec['intellij_extract'],
        creates =>  "${config_home}/config/${key_file}",
        before  =>  File['intellij_license_ownership'],
    }

    file{'intellij_license_ownership':
        path    =>  "${config_home}/config/${key_file}",
        owner   =>  $user,
        group   =>  $group,
        recurse =>  inf,
    }

    file {'intellij_config':
        ensure  =>  present,
        path    =>  "${dir}/bin/${config_filename}",
        content =>  template('idea/vmoptions.erb'),
        owner   =>  $user,
        group   =>  $group,
        require =>  [Exec['intellij_extract'],File['intellij_directory']],
    }

    # Add IDEA to the path
    file {'intellij_binary':
        ensure  =>  link,
        path    =>  "${target}/bin/idea",
        target  =>  "${dir}/bin/idea.sh",
        owner   =>  $user,
        group   =>  $group,
        mode    =>  '0775',
    }
}
