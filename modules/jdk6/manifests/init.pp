# == Class: jdk6
#
# Sets up JDK 6 since IntelliJ IDEA 12 hardcodes the java executable to Java 6 provided by Apple.
#

class jdk6 {
    # For instance this will be 7u17
    $repository = $::charlie_repository

    if $::operatingsystem != 'Darwin' {
        fail('This OS does not need JDK6')
    }

    package {'jdk6':
        ensure => installed,
        source => "${repository}/jdk6_latest.dmg"
    }
}
