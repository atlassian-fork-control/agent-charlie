class postgresql::darwin {
    $tmp = $::config_tmp_directory
    $charlie_dir = $::config_charlie_home
    $repository = $::charlie_repository

    $home = $::config_home
    $user = $::config_user
    $group = $::config_group

    exec {'install_postgresql':
        command => "${charlie_dir}/bin/brew-bottle postgresql",
        creates => '/usr/local/Cellar/postgresql',
    }

    file { 'launch_agent_dir':
        ensure  =>  directory,
        path    =>  "${home}/Library/LaunchAgents",
        owner   =>  $user,
        group   =>  $group,
        recurse =>  inf,
    }

    file {'postgresql_plists':
        ensure  =>  link,
        path    =>  "${home}/Library/LaunchAgents/homebrew.mxcl.postgresql.plist",
        owner   =>  $user,
        group   =>  $group,
        target  =>  '/usr/local/opt/postgresql/homebrew.mxcl.postgresql.plist',
        require =>  [Exec['install_postgresql'], File['launch_agent_dir']],
    }

    exec {'init_postgres_db':
        command =>  "/usr/bin/sudo -u '${user}' /usr/local/bin/initdb /usr/local/var/postgres",
        creates =>  '/usr/local/var/postgres',
        require =>  Exec['install_postgresql'],
    }

    file {'plist_not_group_writable':
        path    =>  '/usr/local/opt/postgresql/homebrew.mxcl.postgresql.plist',
        mode    =>  '0755',
        require =>  Exec['install_postgresql'],
    }

    exec {'load_postgresql_not_root':
        command =>  "/usr/bin/sudo -u '${user}' /bin/launchctl load '${home}/Library/LaunchAgents/homebrew.mxcl.postgresql.plist'",
        require =>  [File['plist_not_group_writable'],File['postgresql_plists']],
    }

    exec {'start_postgresql_not_root':
        command =>  "/usr/bin/sudo -u '${user}' /bin/launchctl start homebrew.mxcl.postgresql",
        require =>  [Exec['load_postgresql_not_root'],Exec['init_postgres_db']],
    }
}
