#!/bin/bash

echo Building Confluence...
if [ -d $HOME/src/confluence ]; then
    rm -rf $HOME/src/confluence
fi

mkdir -p $HOME/.m2/repository
cd $HOME/.m2/repository
curl -L http://agent-charlie.syd.atlassian.com/repository/confluence-repository.tar.bz2 | bzcat | tar -xf -

. ~/.profile
mkdir -p $HOME/src
cd $HOME/src
atlassian-checkout confluence
cd confluence
./scripts/build.rb --install
