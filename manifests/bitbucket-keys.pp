# name: Bitbucket Key Setup
# description: Uploads your keys to bitbucket via the REST endpoint
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         manual
#       manual-desc:    Bitbucket keys uploaded via REST endpoint
#     dependencies:
#       - ssh-keygen
#   Ubuntu:
#     install-method:
#       method:         manual
#       manual-desc:    Bitbucket keys uploaded via REST endpoint
#     dependencies:
#       - ssh-keygen

include bitbucket-keys