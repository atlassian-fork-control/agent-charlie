# name: Atlassian Scripts
# description: A collection of useful scripts
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         manual
#       manual-desc:    Installed to $ATLASSIAN_HOME/atlassian-scripts
#     dependencies:
#       - git
#   Ubuntu:
#     install-method:
#       method:         manual
#       manual-desc:    Installed to $ATLASSIAN_HOME/atlassian-scripts
#     dependencies:
#       - git

include atlassian-scripts