# name: IntelliJ IDEA
# description: IDE for Java, and built on Java
# operatingsystems:
#   Ubuntu:
#     install-method:
#       method:         manual
#       manual-desc:    Installed to $HOME/atlassian/idea13
#     dependencies:
#       - jdk

include idea
