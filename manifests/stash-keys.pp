# name: Stash Key Setup
# description: Generates SSH keys if you don't already have an id_rsa in your .ssh directory
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         manual
#       manual-desc:    Stash keys uploaded via REST endpoint
#     dependencies:
#       - ssh-keygen
#   Ubuntu:
#     install-method:
#       method:         manual
#       manual-desc:    Stash keys uploaded via REST endpoint
#     dependencies:
#       - ssh-keygen

include stash-keys